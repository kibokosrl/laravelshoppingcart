<?php

namespace Kiboko\Shoppingcart\Calculation;

use Kiboko\Shoppingcart\CartItem;
use Kiboko\Shoppingcart\Contracts\Calculator;

class DefaultCalculator implements Calculator
{
    public static function getAttribute(string $attribute, CartItem $cartItem)
    {
        $decimals = config('cart.format.decimals', 2);

        switch ($attribute) {
            case 'discount':
                return round(
                    (
                        $cartItem->getDiscountRate() ? ($cartItem->price * ($cartItem->getDiscountRate() / 100)) : $cartItem->getDiscountFixed()
                    ),
                    $decimals
                );
            case 'tax':
                return round($cartItem->priceTarget * ($cartItem->taxRate / 100), $decimals);
            case 'priceTax':
                return round($cartItem->priceTarget + $cartItem->tax, $decimals);
            case 'discountTotal':
                return round(
                    ($cartItem->discount > 0) ?
                    $cartItem->discount * ($cartItem->getDiscountRate() ? $cartItem->qty : 1) :
                    0,
                    $decimals
                );
            case 'priceTotal':
                return round($cartItem->price > 0 ? $cartItem->price * $cartItem->qty : 0, $decimals);
            case 'priceTotalWt':
                return round($cartItem->priceTotal + ($cartItem->priceTotal * ($cartItem->taxRate / 100)), $decimals);
            case 'subtotal':
                return round($cartItem->priceTotal - $cartItem->discountTotal, $decimals);
            case 'priceTarget':
                return round(($cartItem->priceTotal - $cartItem->discountTotal) / $cartItem->qty, $decimals);
            case 'taxTotal':
                return round($cartItem->subtotal * ($cartItem->taxRate / 100), $decimals);
            case 'total':
                return round($cartItem->subtotal + $cartItem->taxTotal, $decimals);
            default:
                return;
        }
    }
}
