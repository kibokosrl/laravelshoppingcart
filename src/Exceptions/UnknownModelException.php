<?php

namespace Kiboko\Shoppingcart\Exceptions;

use RuntimeException;

class UnknownModelException extends RuntimeException
{
}
