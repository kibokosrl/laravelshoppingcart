<?php

namespace Kiboko\Shoppingcart\Exceptions;

use RuntimeException;

class InvalidCalculatorException extends RuntimeException
{
}
