<?php

namespace Kiboko\Shoppingcart\Exceptions;

use RuntimeException;

class CartAlreadyStoredException extends RuntimeException
{
}
