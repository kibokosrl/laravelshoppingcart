<?php

namespace Kiboko\Shoppingcart\Exceptions;

use RuntimeException;

class InvalidRowIDException extends RuntimeException
{
}
